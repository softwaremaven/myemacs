(defun shell-dwim (&optional create)
  "Start or switch to an inferior shell process, in a smart way. If a buffer with
    a running shell process exists, simply switch to that buffer.  If a shell buffer
    exists, but the shell process is not running, restart the shell.  If already in
    an active shell buffer, switch to the next one, if any. With prefix argument
    CREATE always start a new shell."
  (interactive "P")
  (let* ((next-shell-buffer
          (catch 'found
            (dolist (buffer (reverse (buffer-list)))
              (when (string-match "^\\*ansi-term\\*" (buffer-name buffer))
                (throw 'found buffer))))))
    (if create
        (ansi-term (getenv "SHELL") (generate-new-buffer-name "ansi-term"))
      (visit-term-buffer 't))))
;;      (switch-to-buffer next-shell-buffer))))


(defun visit-term-buffer (&optional use-this-window)
  "Create or visit a terminal buffer."
  (interactive)
  (if (not (get-buffer "*ansi-term*"))
      (progn
        (split-window-sensibly (selected-window))
        (other-window 1)
        (ansi-term (getenv "SHELL")))
    (if use-this-window
         (switch-to-buffer "*ansi-term*")
       (switch-to-buffer-other-window "*ansi-term*"))))

(defadvice erase-buffer (around erase-buffer-noop)
  "make erase-buffer do nothing (used when running shell-command with "
  " background task)")


(defadvice shell-command (around shell-command-unique-buffer activate compile)
  (save-match-data
    (if (or current-prefix-arg
            (not (string-match "[ \t]*&[ \t]*\\'" command)) ;; background
            (bufferp output-buffer)
            (stringp output-buffer))
        ad-do-it ;; no behavior change

      ;; else we need to set up buffer
      (let* ((command-buffer-name
              (format "*background: %s*"
                      (substring command 0 (match-beginning 0))))
             (command-buffer (get-buffer command-buffer-name)))

        (when command-buffer
          ;; if the buffer exists, reuse it, or rename it if it's still in use
          (cond ((get-buffer-process command-buffer)
                 (set-buffer command-buffer)
                 (rename-uniquely))
                ('t
                 (kill-buffer command-buffer))))
        (setq output-buffer command-buffer-name)

        ;; insert command at top of buffer
        (switch-to-buffer-other-window output-buffer)
        (insert "Running command: " command
                "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

        ;; temporarily blow away erase-buffer while doing it, to avoid
        ;; erasing the above
        (ad-activate-regexp "erase-buffer-noop")
        ad-do-it
        (ad-deactivate-regexp "erase-buffer-noop")))))

(provide 'my-shell-utilities)
