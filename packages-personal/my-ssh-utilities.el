
(require 'my-text-utilities)

(defun fix-ssh ()
  "Updates the local SSH connection environment variables."
  (interactive)
  (let ((fix-script (concat (getenv "HOME") "/bin/fixssh")))
    (if (not (file-exists-p fix-script))
        (message "Unable to find fixssh script: %s" fix-script)
      (mapcar
       (lambda (line)
         (if (string-match "export \\(SSH_.+\\)=\\s\"\\(.+\\)\\s\"" line)
             (progn
               (message "Settings %s=%s" (match-string 1 line) (match-string 2 line))
               (setenv (match-string 1 line) (match-string 2 line)))))
       (read-lines fix-script)
       ))))

(provide 'my-ssh-utilities)
