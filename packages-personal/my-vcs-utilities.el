
(defun my-git-grep-escape-files (files)
  "Escape a git-grep files parameter."
  (let ((prefix      (aref files 0))
        (suffix      (aref files (- (length files) 1))))
    (cond
     ((or
       ;; Assume they already escaped things properly
       ;; if it starts and ends with "
       (and (= prefix ?\")  (= suffix ?\"))
       ;; or it starts and ends with '
       (and (= prefix ?')  (= suffix ?'))
       ;; or it has escaped characters in it.
       (string-match "\\\\['*nt\"\\]" files))
      files)
     ((string-match "['*?\n\t\"\\]" files)
      (concat "\""
              (mapconcat (lambda (c)
                   (pcase c
                     (?\n "\\n")
                     (?\t "\\t")
                     (?\\ "\\\\")
                     (?\" "\\\"")
                     (_ (char-to-string c))))
                 files "")
              "\""))
     (t
      files))))

(defun my-maybe-git-rgrep (regexp &optional files dir edit-cmd?)
  "Recursively grep for REGEXP in FILES in directory tree rooted at DIR.
If DIR is in a git repository, we will search using \\[vc-git-grep]; if
DIR is outside of a git repository, we will search using \\[rgrep].
The search is limited to file names matching shell pattern FILES.
FILES may use abbreviations defined in `grep-files-aliases', e.g.
entering `ch' is equivalent to `*.[ch]'.

  With \\[universal-argument] prefix, you can edit the constructed shell command line
before it is executed.
With two \\[universal-argument] prefixes, directly edit and run `grep-command'.

Collect output in a buffer.  While git grep runs asynchronously,
you can use \\[next-error] (M-x next-error), or
\\<grep-mode-map>\\[compile-goto-error] \
in the grep output
buffer, to go to the lines where grep found matches.

This command shares argument histories with \\[rgrep], \\[grep], and
\\[vc-git-grep]."
  (interactive
   (progn
     (grep-compute-defaults)
     (let* ((regexp (grep-read-regexp))
            (files (grep-read-files regexp))
            (dir (if (equal current-prefix-arg '(4))
                     (read-directory-name "Base directory: "
                                          nil default-directory t)
                   "use-right-default"))
            (edit-cmd? (equal current-prefix-arg '(16))))
       (list regexp files dir edit-cmd?))))
  (when (and (stringp regexp) (> (length regexp) 0))
    (let* ((dir      (if (and dir (string= dir "use-right-default"))
                         (or (vc-git-root default-directory)
                             default-directory)
                       (if (and dir (file-directory-p dir) (file-readable-p dir))
                           dir
                         default-directory)))
           (git-root (vc-git-root dir))
           (files    (if git-root
                         (my-git-grep-escape-files files)
                       files)))
      (message "git-root: %s\ndir: %s\nfiles: %s"
               git-root dir files)
      ; Make sure we don't confuse the prefix arg in vc-git-grep.
      (setq current-prefix-arg (if edit-cmd? '(4) nil))
      (if git-root
          (vc-git-grep regexp files dir)
        (rgrep regexp files dir)))))

;; (require 'projectile)
;; (defun my-project-grep ()
;;   (interactive)
;;   )
;;(eval-after-load "" ())

(provide 'my-vcs-utilities)
