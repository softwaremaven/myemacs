
(require 'url-util) ;; for url-hexify-string
(require 'grep)     ;; for grep-tag-default

(with-demoted-errors
    "Failed to load w3m: %s"
    (require 'w3m))

(defvar my-search-internet-url-tmpl "https://duckduckgo.com/?q=<qry-text>"
  "Template URL for performing the search. <qry-text> is replaced with the actual
URL-escaped query content. We rely on features of Duck Duck Go for site-specific
searches.")

(defun my-search-google ()
  "Google the selected region if any, display a query prompt otherwise."
  (interactive)
  (browse-url
   (concat
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q="
    (url-hexify-string (if mark-active
                           (buffer-substring (region-beginning) (region-end))
                         (read-string "Google: "))))))

(defun my-search-query ()
  (read-from-minibuffer "Query: " (grep-tag-default)))

(defun my-search-internet (qry-text)
  "Makes a query using your browser. Uses `my-search-internet-url-tmpl` to
actually open your browser."
  (interactive (list (my-search-query)))
  (browse-url (replace-regexp-in-string "<qry-text>" (url-hexify-string qry-text)
                                        my-search-internet-url-tmpl)))

(defun my-search-so (qry-text)
  "Searches Stack Overflow for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!so " qry-text)))

(defun my-search-wikipedia (qry-text)
  "Searches Wikipedia for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!w " qry-text)))

(defun my-search-emacs (qry-text)
  "Searches EmacsWiki for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!emacs " qry-text)))

(defun my-search-w3schools (qry-text)
  "Searches w3schools for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!w3schools " qry-text)))

(defun my-search-hn (qry-text)
  "Searches Hacker News for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!hn " qry-text)))

(defun my-search-apple (qry-text)
  "Searches Apple Developer Center for the given text"
  (interactive (list (my-search-query)))
  (my-search-internet (concat "!adc " qry-text)))

(defun proxy-up ()
  (interactive)
  (setq url-proxy-services
        '(("no_proxy" . "^\\(localhost\\|.*us\.oracle\.com\\)")
          ("http" . "www-proxy.us.oracle.com:80")
          ("https" . "www-proxy.us.oracle.com:80")))
  (mapc (lambda (s)
          (let ((var (car s))
                (val (cadr s)))
            (setenv (car s) (cadr s))
            (setq w3m-command-arguments
                  (nconc w3m-command-arguments
                         '("-o" (format "%s=%s" var val))))))
        '(("no_proxy" "localhost,.us.oracle.com")
          ("http_proxy" "www-proxy.us.oracle.com:80")
          ("https_proxy" "www-proxy.us.oracle.com:80"))))

(defun proxy-down ()
  (interactive)
  (setq url-proxy-services nil)
  (setq w3m-command-arguments nil)
  (mapc 'setenv '("no_proxy" "http_proxy" "https_proxy" "socks_proxy" "all_proxy" "ALL_PROXY")))

(provide 'my-internet-utilities)
