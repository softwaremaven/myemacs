
(defcustom search-all-buffers-ignored-files (list (rx-to-string '(and bos (or ".bash_history" "TAGS") eos)))
  "Files to ignore when searching buffers via \\[search-all-buffers]."
  :type 'editable-list
  :group 'search-buffers)

(require 'grep)
(defun search-all-buffers (regexp prefix)
  "Searches file-visiting buffers for occurence of REGEXP.  With
prefix > 1 (i.e., if you type C-u \\[search-all-buffers]),
searches all buffers."
  (interactive (list (grep-read-regexp)
                     current-prefix-arg))
  (message "Regexp is %s; prefix is %s" regexp prefix)
  (multi-occur
   (if (member prefix '(4 (4)))
       (buffer-list)
     (remove-if
      (lambda (b) (some (lambda (rx) (string-match rx  (file-name-nondirectory (buffer-file-name b)))) search-all-buffers-ignored-files))
      (remove-if-not 'buffer-file-name (buffer-list))))

   regexp))

;; from http://www.jwz.org/doc/tabs-vs-spaces.html
(defun untabify-buffer ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "[ \t]+$" nil t)
      (delete-region (match-beginning 0) (match-end 0)))
    (goto-char (point-min))
    (if (search-forward "\t" nil t)
        (untabify (1- (point)) (point-max))))
  nil)

(defun newline-below-point (&optional arg column)
  "Insert a newline and move the cursor to the same point it was on the
previous line.
With ARG, insert that many newlines.
With COLUMN, space to that column instead of the column the cursor was on."
  (interactive "*P")
  (barf-if-buffer-read-only)
  (let ((col (if (eq column nil)
                 (current-column)
               column))
        (to-insert (if (eq arg nil)
                       0
                     (if (number-or-marker-p arg)
                         arg
                       (prefix-numeric-value arg)))))
    (newline)
    (insert-char ?\s col)
    (when (> to-insert 0)
      (newline-below-point (- to-insert 1) col))
    )
)

(defun switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(defun do-for-all-buffers (func &optional predicate inhibit-detail)
  "Perform the given FUNC for every buffer. If PREDICATE is non-nil,
only perform the operations on buffers matching PREDICATE. Both FUNC
and PREDICATE should accept no arguments. Instead, they can rely on
(current-buffer) being set.

When INHIBIT-DETAIL is nil, we will return a list of cons cells containing
the buffer name, the file name, and the value returned by FUNC. If INHIBIT-DETAIL
is non-nil, we will only return the values returned by FUNC."
  (let ((responses nil))
    (dolist (frame (frame-list) responses)
      (dolist (buffer (append (frame-parameter frame 'buffer-list)
                              (frame-parameter frame 'buried-buffer-list)))
        (with-current-buffer buffer
          (when (or (not predicate) (funcall predicate))
            (let ((value (when func (funcall func))))
              (setq responses
                    (cons
                     (if (not inhibit-detail)
                        (list (cons "buffer-name" (buffer-name))
                              (cons "file-name" (buffer-file-name))
                              (cons "value" value))
                       value)
                     responses)))))))))

(defsubst print-for-all-buffers (func &optional predicate)
  "Prints the result of FUNC for every buffer. If PREDICATE is non-nil,
only perform the operations on buffers matching PREDICATE. Both
FUNC and PREDICATE should accept no arguments; (current-buffer)
will be set appropriately. FUNC should return something that can
be formatted by `format`."
  (lexical-let ((func func)
                (predicate predicate))
    (do-for-all-buffers (lambda () (message "%s" (funcall func))) predicate t)))

(provide 'my-buffer-utilities)
