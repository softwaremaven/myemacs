

(defun my-yank-primary ()
  "Yanks the contents of the primary selection at the current point."
  ;; Cribbed from `mouse-yank-primary` in mouse.el.gz.
  (interactive)
  ;; Without this, confusing things happen upon e.g. inserting into
  ;; the middle of an active region.
  (when select-active-regions
    (let (select-active-regions)
      (deactivate-mark)))
  (let ((primary
         (if (fboundp 'x-get-selection-value)
             (if (eq (framep (selected-frame)) 'w32)
                 ;; MS-Windows emulates PRIMARY in x-get-selection, but not
                 ;; in x-get-selection-value (the latter only accesses the
                 ;; clipboard).  So try PRIMARY first, in case they selected
                 ;; something with the mouse in the current Emacs session.
                 (or (x-get-selection 'PRIMARY)
                     (x-get-selection-value))
               ;; Else MS-DOS or X.
               ;; On X, x-get-selection-value supports more formats and
               ;; encodings, so use it in preference to x-get-selection.
               (or (x-get-selection-value)
                   (x-get-selection 'PRIMARY)))
           ;; FIXME: What about xterm-mouse-mode etc.?
           (x-get-selection 'PRIMARY))))
    (unless primary
      (error "No selection is available"))
    (push-mark (point))
    (insert-for-yank primary)))

(provide 'my-selection-utilities)
