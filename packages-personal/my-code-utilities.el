(require 'my-elisp-utilities)
(require 'my-file-utilities)

;; Provide a nice way to remove pesky ^M's
(defun remove-dos-eol ()
  "Removes the disturbing '^M' showing up in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))


(defun my-uncamelize (s &optional sep start)
  "Convert CamelCase string S to lower case with word separator SEP.
    Default for SEP is an underscore \"_\".
    If third argument START is non-nil, convert words after that
    index in STRING."
  (let ((case-fold-search nil))
    (while (string-match "[A-Z]" s (or start 1))
      (setq s (replace-match (concat (or sep "_")
                                     (downcase (match-string 0 s)))
                             t nil s)))
    (downcase s)))

(defun my-camelize (s)
  "Convert under_score string S to CamelCase string."
  (mapconcat 'identity (mapcar
                        #'(lambda (word) (capitalize (downcase word)))
                        (split-string s "_")) ""))

(defun my-camelize-method (s)
  "Convert under_score string S to camelCase string."
  (mapconcat 'identity (mapcar-head
                        '(lambda (word) (downcase word))
                        '(lambda (word) (capitalize (downcase word)))
                        (split-string s "_")) ""))

(defvar my-cltests-args "" "Arguments to pass to clicklock_tests")
(defvar my-cltests-history 'nil)

(defvar my-dbg-file "" "What file am I debugging?")

(defun my-get-gdb-file ()
  "Gets the file to debug"
  (setq my-dbg-file
        (read-file-name "File to debug: "
                        (if (boundp 'my-dbg-file)
                            (file-name-directory my-dbg-file)
                          default-directory)
                        (if (boundp 'my-dbg-file)
                            my-dbg-file
                          "")
                        nil
                        (if (boundp 'my-dbg-file)
                            (file-name-nondirectory my-dbg-file)
                          nil))))

(defun my-gdb (&optional FILE)
  "Open gdb on FILE in many-windows mode in a new frame (or returns to an existing session)"
  (interactive)
  (let ((dbg-file (if FILE FILE (my-get-gdb-file))))
    (condition-case nil
        (progn
          (setq gdb-many-windows t)
          (select-frame-by-name  "my-gdb-frame"))
      (error
       (make-frame '((name . "my-gdb-frame") (width . 160) (height . 50)))
       (gdb (concat "gdb --annotate=3 " dbg-file))
       (set-process-sentinel
        (get-process (concat "gud-" (file-name-nondirectory dbg-file)))
        (lambda (process event)
          (if (string-match "finished" event)
              (delete-frame (select-frame-by-name  "my-gdb-frame")))))))))

(defun smart-open-line ()
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun open-python-stack-entry ()
  "Opens the file under the point in a Python stack trace and moves to
the appropriate line number"
  (interactive)
  (save-excursion
    (let ((boundaries (bounds-of-thing-at-point 'line)))
      (let ((cur-line (buffer-substring-no-properties
                     (car boundaries) (cdr boundaries))))
        (progn
          (message cur-line)
          (if (or (string-match "File \"\\(.*?\\)\", line \\([[:digit:]]+\\)," cur-line)
                  (string-match "> \\(.*?\\)(\\([[:digit:]]+\\)).*" cur-line))
              (let ((file-name (match-string-no-properties 1 cur-line))
                    (line-num (string-to-number (match-string-no-properties 2 cur-line))))
                (let ((buf (find-file file-name)))
                      (progn
                        (switch-to-buffer-other-window  buf)
                        ;; (display-buffer  buf '((display-buffer-use-some-window
                        ;;                         display-buffer-use-some-window)
                        ;;                        (reusable-frames . 'visible)))
                        (goto-line line-num))))
            (message "Couldn't parse file information. Are you on a file path?")
          ))))))

(defun my-compile-func ()
  "This function does a compile."
  (interactive)
  (compile (format "make -C %s" (file-name-directory (get-closest-pathname)))))

(defun my-compile-clean-func ()
  "This function does a clean compile."
  (interactive)
  (compile (format "make -C %s clean" (file-name-directory (get-closest-pathname)))))

(defun my-compile-package-func ()
  "This function builds an Endura package."
  (interactive)
  (compile (format "make -C %s package" (file-name-directory (get-closest-pathname)))))


(provide 'my-code-utilities)
