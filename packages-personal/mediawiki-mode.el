(define-generic-mode 'mediawiki-mode
  ; comments
  nil
  ; keywords
  nil
  ; font-lock list
  '(("^\\(= \\)\\(.*?\\)\\($\\| =$\\)" . 'info-title-1)
    ("^\\(== \\)\\(.*?\\)\\($\\| ==$\\)" . 'info-title-2)
    ("^\\(=== \\)\\(.*?\\)\\($\\| ===$\\)" . 'info-title-3)
    ("^\\(====+ \\)\\(.*?\\)\\($\\| ====+$\\)" . 'info-title-4)
    ("\\[\\[.*?\\]\\]" . 'link)
    ("<nowiki>.*?</nowiki>" . font-lock-doc-face)
    ("<u>.*?</u>" . 'underline)
    ("<pre>.*?</pre>" . font-lock-reference-face)
    ("<code>.*?</code>" . font-lock-type-face)
    ("<tt>.*?</tt>" . font-lock-type-face)
    ("<!--.*?-->" . font-lock-comment-face)
    ("\\_</.*?/" . 'italic)
    ("|+=?" . font-lock-string-face)
    ("'''.*?'''" . 'bold)
    ("''.*?''" . 'italic)

    ("\\\\\\\\[ \t]+" . font-lock-warning-face))
  ; auto-mode-alist
  '(".mwiki\\'")
  ; function-list
  '((lambda () (require 'info) (require 'goto-addr)))
  "Media Wiki mode.")

(provide 'mediawiki-mode)
