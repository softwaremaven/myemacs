
(defun open-notes (date)
  (interactive "sDate: ")
  (let ((filename (concat
                   (file-name-as-directory my-notes-dir)
                   (format "notes-%s.org" date))))
       (message filename)
       (find-file filename)
  ))

(defun todays-notes ()
  (interactive)
  (open-notes (format-time-string "%Y-%m-%d")))

(provide 'my-note-utilities)
