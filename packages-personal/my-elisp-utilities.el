;; (defun mk-single-backtrace-advice (function)
;;   (defadvice flymake-mode (before flymake-mode-backtrace-advice activate)
;;     (message-show-backtrace)
;;     (ad-deactivate function))
;;   (ad-activate function)
;;   )

(defun remove-elc-on-save ()
  "If you're saving an elisp file, likely the .elc is no longer valid."
  (add-hook 'after-save-hook
            (lambda ()
              (if (file-exists-p (concat buffer-file-name "c"))
                  (delete-file (concat buffer-file-name "c"))))
            nil
            t))

(defun indent-defun ()
  "Indent the current defun."
  (interactive)
  (save-excursion
    (mark-defun)
    (indent-region (region-beginning) (region-end))))

(defun mapcar-head (fn-head fn-rest list)
  "Like MAPCAR, but applies a different function to the first element."
  (if list
      (cons (funcall fn-head (car list)) (mapcar fn-rest (cdr list)))))


(defun message-show-backtrace (msg)
  "Shows the backtrace. It really should have a way to specify 'only once'
on a per-function basis."
  ;; Why don't we get a real backtrace? *sigh*
  (message "Backtrace: %s\n%s" msg (backtrace-frame 10)))

;; We could make this a macro!
;; (def-dbg-advice (function)
;;     (defadvice (get-symbol-from-function function)
;;     ...)

(defadvice flymake-mode (before flymake-mode-backtrace-advice activate)
  ;; Until I can figure out why we don't get a real backtrace, use
  (debug)
  ;; and start with --debug-init
  ;; (message-show-backtrace "flymake")
  (ad-deactivate 'flymake-mode)
  )

(defun recompile-init ()
  "Byte-compile all your dotfiles again."
  (interactive)
  (byte-recompile-directory dotfiles-dir 0))

(defun eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))


(provide 'my-elisp-utilities)
