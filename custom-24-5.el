(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(allout-auto-activation t)
 '(allout-command-prefix "")
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (wheatgrass)))
 '(desktop-clear-preserve-buffers
   (quote
    ("\\*scratch\\*" "\\*Messages\\*" "\\*server\\*" "\\*tramp/.+\\*" "\\*Warnings\\*" "\\*web\\*" "\\*couchdb\\*" "\\*shell\\*" "\\*shell\\*<.+>" "\\*ansi-term\\*.*" "\\*terminal\\*.*")))
 '(egg-mode-key-prefix "C-c v")
 '(flycheck-flake8rc "/home/travis/src/updates-server/etc/flake8.conf")
 '(frame-background-mode (quote dark))
 '(inhibit-startup-buffer-menu t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(initial-scratch-message nil)
 '(ispell-highlight-face (quote flyspell-incorrect))
 '(ns-command-modifier (quote meta))
 '(org-babel-load-languages
   (quote
    ((sqlite . t)
     (python . t)
     (shell . t)
     (emacs-lisp . t))))
 '(python-indent-guess-indent-offset t)
 '(racket-program "/Users/travis/opt/Racket/bin/racket")
 '(raco-program "/Users/travis/opt/Racket/bin/raco")
 '(safe-local-variable-values
   (quote
    ((eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("*/logs/*" "virtualenvs/*" "test_reports/*"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" "virtualenvs" "test_reports"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("logs/*" ".cache/*" "virtualenvs/*" "test_reports/*" ".coverage*" "*.html"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (ffip-project-root "~/src/updates-server")
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("*/logs/*" "*/.cache/*" "*/virtualenvs/*" "*/test_reports/*" "*/.coverage*" "*.html"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("*/logs/*" "*/.cache/*" "*/virtualenvs/*" "*/test_reports/*" "*/.coverage*" "*.html"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("*/logs/*" "*/.cache/*" "*/virtualenvs/*" "*/test_reports/*" "*/.coverage*"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("logs/*" ".cache/*" "virtualenvs/*" "test_reports/*" ".coverage*"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (eval progn
           (require
            (quote find-file-in-project))
           (setq-local ffip-prune-patterns
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        ffip-prune-patterns))
           (require
            (quote elpy))
           (setq-local elpy-project-ignored-directories
                       (append
                        (quote
                         ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
                        elpy-project-ignored-directories)))
     (ffip-find-executable . "find -L")
     (eval setq-local elpy-project-ignored-directories
           (append
            (quote
             ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
            elpy-project-ignored-directories))
     (eval setq-local ffip-prune-patterns
           (append
            (quote
             ("logs" ".cache" "virtualenvs" "test_reports" ".coverage*"))
            ffip-prune-patterns))
     (ffip-find-options . "-not -size +64k"))))
 '(show-paren-mode t)
 '(show-trailing-whitespace t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(error ((t (:foreground "brightred" :weight bold))))
 '(highlight-indentation-face ((t (:background "gray20"))))
 '(minibuffer-prompt ((t (:foreground "light blue"))))
 '(mode-line-inactive ((t (:background "grey20" :foreground "grey90" :box (:line-width -1 :color "grey75") :weight light))))
 '(racket-paren-face ((t (:foreground "light slate gray"))))
 '(smerge-refined-added ((t (:inherit smerge-refined-change :background "color-22"))))
 '(smerge-refined-removed ((t (:inherit smerge-refined-change :background "color-124")))))
