(message "Time to initialize")
;; Directory of init files
(setq dotfiles-dir (file-name-directory
                    (or (buffer-file-name) load-file-name)))
(setq emacs-version-short (replace-regexp-in-string
                           "\\([0-9]+\\)\\.\\([0-9]+\\).*"
                           "\\1-\\2" emacs-version)) ; 25.0.50.1 -> 25-0

(defun my-init-path (&optional internal-path)
  "Returns the path of the initialization files. If `internal-path` is provided, it
will be added to the returned path. Examples:
  (my-init-path)                      --> \"/home/travis/.emacs.d\"
  (my-init-path \"snippets/foo/bar\") --> \"/home/travis/.emacs.d/snippets/foo/bar\""
  (if internal-path
      (concat dotfiles-dir (s-chop-prefix "/" internal-path))
    dotfiles-dir))

;; Custmomizations

(message "Setting up custom variables and faces")
(setq custom-file (expand-file-name
                   (concat "custom-" emacs-version-short ".el")
                   user-emacs-directory))
(load custom-file :noerror :nomessage)

;; Standard load paths
(add-to-list 'load-path (concat dotfiles-dir "my-init-files"))
(add-to-list 'load-path (concat dotfiles-dir "packages"))
(add-to-list 'load-path (concat dotfiles-dir "packages-personal"))

;; Some debug information if needed.
(defvar my-debugging-p nil "Enable debugging information")
(defvar my-debugging--require-depth 0 "Recursive require depth")
(defun my-debug (format &rest args)
  (when my-debugging-p
    (apply 'message format args)))
(defvar my-require-debugging-ignored
  '("help-fns" "bytecomp" "help-mode" "cl-lib" "info" "info-look"))
(defun my-require-debugging-p (args)
  (and my-debugging-p
       (let ((required (if (symbolp args) (symbol-name args)
                         (if (stringp args) args
                           nil))))
         (if required
             (not (member required my-require-debugging-ignored))
           (message "Unknown require arguments (%s) of type (%s) for require debugging. Ignoring."
                    args (type-of args))
           nil))))
(defun my-require-debugging-advice (orig-fun &rest args)
  (let* ((required-module (car args))
         (should-log-it (my-require-debugging-p required-module))
         (depth my-debugging--require-depth)
         (depth-str (format (format "%%%ss" depth) " ")))
    (when should-log-it
      (message "%sstart-require: %s" depth-str required-module))
    (setq my-debugging--require-depth (+ depth 1))
    (let ((result (apply orig-fun args)))
      (when should-log-it
        (message "%send-require: %s" depth-str required-module))
      (setq my-debugging--require-depth depth)
      result)))
(advice-add 'require :around 'my-require-debugging-advice)


(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  ;; add-to-list prepends (without the append flag) so put them in reverse order
  ;; (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
  (add-to-list 'package-archives '("orgmode" . "http://orgmode.org/elpa/"))
  ;; (add-to-list 'package-archives '("elpy" . "http://jorgenschaefer.github.io/packages/"))
)

;; Init-0 functions
(require 'my-init-0)

(add-to-exec-path "/usr/local/bin")
(add-to-env-path "PATH" "/usr/local/bin")

;; Conditional Loading for Platforms
(message "Loading platform-specific configuration")
(cond
 ((string-match "linux" system-configuration)
  ; Hey, its linux, try to load the linux customizations
  (progn
    (defconst my-platform "linux")
    (if (file-exists-p (concat dotfiles-dir "platform/my-linux.el"))
        (progn
          (message "loading GNU Emacs customizations for Linux")
          (load-file (concat dotfiles-dir "platform/my-linux.el"))))))
 ((string-match "apple" system-configuration)
 ; It's OS X, load those customizations
  (progn
    (defconst my-platform "osx")
    (if (file-exists-p (concat dotfiles-dir "platform/my-osx.el"))
        (progn
          (message "loading GNU Emacs customizations for Apple")
          (load-file (concat dotfiles-dir "platform/my-osx.el"))))))
 (t
  (progn
    (defconst my-platform "undefined")
    (message "did not load a platform configuration"))))


;; Conditional Loading for Machines
(message "Loading machine-specific configuration")
(let ((local-conf-name (concat dotfiles-dir
                               (format "platform/my-host-%s.el"
                                       (car (split-string (system-name) "\\."))))))
  (cond ((file-exists-p local-conf-name)
         (load-file local-conf-name))
        ((message "%s doesn’t exist or I’d load it." local-conf-name))))

;; Personal customizations to existing packages
(message "Seeding package-specific configuration")
(require 'my-generic) ;; first

(message "Loading package configurations")
(require 'generic-x)

;;(require 'my-allout)
(require 'my-buffers)
(require 'my-chords)
(require 'my-coffee)
(require 'my-comint)
;;(require 'my-compile)
(require 'my-docker)
;;(require 'my-edit-server)
(require 'my-erc)
;;(require 'my-erlang)
;;(require 'my-flymake)
(require 'my-files)
;;(require 'my-go)
(require 'my-git)
(require 'my-html)
(require 'my-ido)
(require 'my-js2)
(require 'my-lisp)
(require 'my-logging)
;;(require 'my-objc)
(require 'my-org)
(require 'my-prog-mode)
(require 'my-projectile)
(require 'my-python)
(require 'my-shell)
(require 'my-tags)
(require 'my-text)
(require 'my-tramp)
(require 'my-web)
(require 'my-wiki)
(require 'my-windmove)
(require 'my-xml)
(require 'my-yaml)
(require 'my-yas)

(put 'scroll-left 'disabled nil)

(defun my-load-desktop ()
  ;; Now that everything is loaded and we have a frame, let's restore
  ;; our desktop.
  (message "Loading my desktop")
  (require 'my-desktop))

(add-frame-init-hook 'my-load-desktop t)

(message "Finished initializing")
