(message "Loading Apple configuration")
(defconst my-platform "osx")

(modify-frame-parameters (selected-frame) '((alpha . 85)))
(setq initial-frame-alist '((left . 1) (width . 202) (height . 54) (top . 22)))
;;(smart-frame-positioning-mode nil)

;; Variables defining locations of things that don't have a whole mode
(setq-default ispell-program-name "/usr/local/bin/aspell")

(cua-mode 0)

(add-to-exec-path "/usr/local/bin")
(add-to-env-path "PATH" "/usr/local/bin")
(add-to-exec-path "/usr/local/share/npm/bin")
(add-to-env-path "PATH" "/usr/local/share/npm/bin")

(setq inferior-erlang-machine "/usr/local/bin/erl")

(setq Info-directory-list
      ;; TODO: Get the emacs info dir programatically
      (list "/usr/local/share/info/emacs/"
            ;; "/usr/local/Cellar/emacs/25.0-dev/share/info/emacs/"
            "/usr/local/share/info/"
            "/usr/local/info/"
            "/usr/local/gnu/info/"
            "/usr/local/gnu/lib/info/"
            "/usr/local/gnu/lib/emacs/info/"
            "/usr/local/emacs/info/"
            "/usr/local/lib/info/"
            "/usr/local/lib/emacs/info/"
            "/usr/share/info/"
            "/Users/travis/info"))

(setq MY-PYTHONPATH "/Applications/MacPorts/Emacs.app/Contents/Resources/etc")
(add-to-exec-path "/Users/travis/bin")
(add-to-env-path "PATH" "/Users/travis/bin")

(setq-default generic-define-unix-modes 't)

(setq-default my-notes-dir "/Users/travis/Documents/notes")

;;(one-buffer-one-frame-mode 0)
;;(define-key dired-mode-map "o" 'dired-open-mac)
;;(defun dired-open-mac ()
;;  (interactive)
;;  (let ((file-name (dired-get-file-for-visit)))
;;    (if (file-exists-p file-name)
;;        (call-process "/usr/bin/open" nil 0 nil file-name))))


;;; for Aquamacs Emacs, they set some keys in
;;; the osx-key-mode-map, and make it a more
;;; default keymap than global-map.
;;(if (boundp 'osx-key-mode-map)
;;    (setq hah-key-map osx-key-mode-map)
;;  (setq hah-key-map global-map))

;;(define-key hah-key-map [home]   'beginning-of-line)
;;(define-key hah-key-map [end]    'end-of-line)
;;(define-key hah-key-map [C-home] 'beginning-of-buffer)
;;(define-key hah-key-map [C-end]  'end-of-buffer)
