(message "Loading Balin customizations")

(defvar mytags-directories `(("ctag" "/Users/travis/src/uln/src")
;;                             ("etag" "/Users/travis/src/pivot/pivot-mobile")
                             ("ctag" ,dotfiles-dir)
                             )
  "List of directories to generate tags for")

(setq tramp-default-proxies-alist nil)
(add-to-list 'tramp-default-proxies-alist '("nefarian"  "\\`root\\'" "/ssh:nefarian:"))
(add-to-list 'tramp-default-proxies-alist '("ragnoros" "\\`root\\'" "/ssh:ragnoros:"))
(add-to-list 'tramp-default-proxies-alist '("thorin" "\\`root\\'" "/ssh:angel:"))

;;(add-to-list 'tramp-default-proxies-alist
;;                  '("\\.your\\.domain\\'" "\\`root\\'" "/ssh:%h:"))
