(message "Loading Ksplice customizations")

(defvar mytags-directories `(("ctag" "/home/travis/ksplice-status")
                             ("ctag" ,dotfiles-dir)
                             )
  "List of directories to generate tags for")

(defvar tramp-default-proxies-alist nil)
(add-to-list 'tramp-default-proxies-alist '("ksplice" "\\`root\\'" "/ssh:travis@ksplice.us.oracle.com:"))
(add-to-list 'tramp-default-proxies-alist '("shell1" "\\`root\\'" "/ssh:travis@ksplice-shell1.us.oracle.com:"))
(add-to-list 'tramp-default-proxies-alist '("shell2" "\\`root\\'" "/ssh:travis@ksplice-shell2.us.oracle.com:"))
