(message "Loading Linux configuration")

;; Variables defining locations of things
(setq-default ispell-program-name "/usr/bin/aspell")

(setq-default generic-define-unix-modes 't)

(setq-default my-notes-dir "/home/travis/Documents/notes")

;; Start up the gnu client/server stuff
;(load "gnuserv-compat")
;(load "gnuserv")
;(gnuserv-start)

(add-to-exec-path "/home/travis/bin")
(add-to-env-path "PATH" "/home/travis/bin")

(require 'cl)
;;(unless (boundp 'mouse-wheel-mode)
;;  (defvar mouse-wheel-mode nil "Needed for centered-cursor-mode"))

(defun my-setup-window-system ()
  (case window-system
        (x
         (progn
           (message "Setting up for running in X")
           (setq x-select-enable-clipboard t)
           (setq interprogram-paste-function 'x-cut-buffer-or-selection-value)))
        (otherwise
         (progn
           (message "Setting up for running on terminal")
           (menu-bar-mode 0)))))

(add-frame-init-hook 'my-setup-window-system)

(setq Info-directory-list (list "/usr/share/info/" "/usr/local/share/info/"))

(defcustom ssh-agent-buffer "*ssh-agent*"
  "buffer to display ssh-agent output in"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-agent-program "ssh-agent"
  "ssh-agent program"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-agent-args ""
  "ssh-agent program arguments"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-add-program "ssh-add"
  "ssh-add program"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-add-args ""
  "ssh-add program arguments"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-add-prompt "Enter passphrase for \\([^:]+\\):"
  "ssh-add prompt for passphrases"
  :type 'string
  :group 'ssh-agent)

(defcustom ssh-add-invalid-prompt "Bad passphrase, try again:"
  "ssh-add prompt indicating an invalid passphrase"
  :type 'string
  :group 'ssh-agent)

(defun ssh-agent ()
  "execute the ssh-agent"
  (interactive)
  (let ((args (split-string ssh-agent-args)))
    (set-process-filter
     (apply #'start-process "ssh-agent" nil ssh-agent-program args)
     #'ssh-agent-process-filter)))

(defun ssh-add-run ()
  "run ssh-add"
  (let ((args (split-string ssh-add-args)))
    (set-process-filter
     (apply #'start-process "ssh-add" nil ssh-add-program args)
     #'ssh-add-process-filter)))

(defun ssh-agent-process-filter (process input)
  "filter for ssh-agent input"
  (cond ((ssh-agent-read-var "SSH_AUTH_SOCK" input)
         (ssh-add-run))
        ((ssh-agent-read-var "SSH_AGENT_PID" input))))

(defun ssh-agent-read-var (var line)
  "read a shell script variable from ssh-agent's output"
  (if (string-match (format "%s[= ]\\([^;]+\\)" var) line)
      (with-current-buffer (get-buffer-create ssh-agent-buffer)
        (let ((value (match-string 1 line)))
          (setenv var value)
          (insert line))
        t)))

(defun ssh-add-process-filter (process input)
  "filter for ssh-add input"
  (cond ((string-match ssh-add-prompt input)
         (ssh-send-passwd process input))
        ((string-match ssh-add-invalid-prompt input)
         (ssh-send-passwd process input))
        (t (with-current-buffer (get-buffer-create ssh-agent-buffer)
             (insert input)))))

(defun ssh-send-passwd (process prompt)
  "read a password with `read-passwd` and pass it to the ssh-add process"
  (let ((passwd (read-passwd prompt)))
    (process-send-string process passwd)
    (process-send-string process "\n")
    (clear-string passwd)))
