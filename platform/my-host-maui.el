
(message "Loading custom settings for maui")

;; On Maui, html gets django-mode
(setq auto-mode-alist (cons '("\\.html$" . django-html-mode) auto-mode-alist))

;; Hook to rsync to the server? Maybe not...
;;(add-hook 'after-save-hook 
;;          '(lambda () (define-key python-mode-map "\C-m" 'newline-and-indent)))
