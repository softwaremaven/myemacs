
(require 'tramp)
;; rsync is significantly faster than ssh, but doesn't support tramp proxies.
;;(setq tramp-default-method "ssh")
(setq tramp-default-method "ssh") 
(provide 'my-tramp)
