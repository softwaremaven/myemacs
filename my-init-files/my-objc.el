;; Objective C stuff
;; We are checking to see if an objective C, a C++, or a C file exists
;; to match the header.  If we can't find one, then default to C++ mode
(defun objc-choose-header-mode ()
    (interactive)
    (if (string-equal (substring (buffer-file-name) -2) ".h")
        (progn
        ;; OK, we got a .h file, if a .m file exists we'll assume it's
        ;; an objective c file. Otherwise, we'll look for a .cpp file.
            (let ((dot-m-file (concat (substring (buffer-file-name) 0 -1) "m"))
                  (dot-mm-file (concat (substring (buffer-file-name) 0 -1) "mm"))
                  (dot-cpp-file (concat (substring (buffer-file-name) 0 -1) "cpp"))
                  (dot-c-file (concat (substring (buffer-file-name) 0 -1) "c")))
                  (cond
                    ((or (file-exists-p dot-m-file) (file-exists-p dot-mm-file))
                        (progn
                            (objc-mode)))
                    ((file-exists-p dot-cpp-file)
                        (c++-mode))
                    ((file-exists-p dot-c-file)
                        (c-mode))
                    (t
                        (progn
                            (c++-mode))))))))


(add-hook 'find-file-hook 'objc-choose-header-mode)

;; Make sure .mm (Objective C with C++) files show up as objective C
(setq auto-mode-alist (cons '("\\.mm" . objc-mode) auto-mode-alist))

(provide 'my-objc)