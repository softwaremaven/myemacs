(require 'uniquify)
(require 'ibuffer)
(require 'ibuf-ext)

(require 'my-buffer-utilities)

(setq ibuffer-formats
      '(
        (mark modified read-only " " (name 23 23 :left :elide) " "
              (mode 15 15 :left :elide) " " filename-and-process)
        (mark vc-status-mini modified read-only " "
              (project 30 30 :left :elide) " " name)
        (mark modified read-only " " (name 25 25 :left :elide) " "
              (mode 9 9 :left :elide) " " filename-and-process)
        (mark modified read-only " " (name 20 20 :left :elide) " "
              (size-h 6 -1 :right) " " (mode 16 16 :left :elide) " "
              filename-and-process)
        (mark  modified " " (name 24 -1) " " filename)
        (mark  modified " " (name 24 -1) " " filename-and-process)
        ))

;;(add-to-list 'ibuffer-never-show-predicates "^\\*")
(defvar-local my-ibuffer-project-name nil "project name buffer is in")

;; Switching to ibuffer puts the cursor on the most recent buffer
(defadvice ibuffer (around ibuffer-point-to-most-recent) ()
  "Open ibuffer with cursor pointed to most recent buffer name"
  (let ((recent-buffer-name (buffer-name)))
    ad-do-it
    (ibuffer-jump-to-buffer recent-buffer-name)))
(ad-activate 'ibuffer)

(defadvice ibuffer-switch-format (after ibuffer-switch-format-maybe-show-processes) ()
  "Do we show processes in this mode? If not, add the process filter "
  "to ibuffer-tmp-hide-regexps."
  (let ((process-regex "^\\*"))
    (if (and
         (local-variable-p 'ibuffer-current-format)
         (not (eq ibuffer-current-format nil))
         (or (= ibuffer-current-format 1)
             (= ibuffer-current-format 4)))
        (when (not (member process-regex ibuffer-tmp-hide-regexps))
          (ibuffer-add-to-tmp-hide process-regex))
      (when ibuffer-tmp-hide-regexps
        (setq-local ibuffer-tmp-hide-regexps
                    (remove process-regex ibuffer-tmp-hide-regexps)))))
  (ibuffer-update nil t))
(ad-activate 'ibuffer-switch-format)

;; BODY will be called with `buffer' bound to the buffer object, and
;; `mark' bound to the current mark on the buffer.  The original ibuffer
;; buffer will be bound to `ibuffer-buf'.
(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
   ((> (buffer-size) 1000000) (format "%4.1fM" (/ (buffer-size) 1000000.0)))
   ((> (buffer-size) 100000) (format "%4.0fk" (/ (buffer-size) 1000.0)))
   ((> (buffer-size) 1000) (format "%4.1fk" (/ (buffer-size) 1000.0)))
   (t (format "%4d" (buffer-size)))))

(defun my-buffers--desc-from-path (path dir-map)
  (if dir-map
      (let ((map-item (car dir-map)))
        (if (s-contains? (car map-item) path)
            (cdr map-item)
          (my-buffers--desc-from-path path (cdr dir-map))))
    nil))

(defun my-buffers-updates-subproj (path)
  (let* ((dir-map '(("shared/python" . "shared")
                    ("shared/ksserver" . "client")
                    ("tests/python" . "shared:tests")
                    ("tests/client" . "client:tests")
                    ("apps/updates-webapp/tests" . "webapp:tests")
                    ("apps/updates-webapp" . "webapp")
                    ("apps/authenticator-map/tests" . "authn:tests")
                    ("apps/authenticator-map" . "authn")
                    ("tools/tester/tests" . "tester:tests")
                    ("tools/tester" . "tester")
                    ("etc" . "config")
                    ("virtualenvs" . "venvs")
                    ("bin" . "scripts")
                    ))
         (mapped-name (my-buffers--desc-from-path path dir-map)))
    (if mapped-name (format "updates-server:%s" mapped-name)
      "updates-server")))

(defun my-buffers-project-for-buffer (&optional buffer)
  "Determines the project the currently bound buffer is a member of"
  (unless my-ibuffer-project-name
    (setq my-ibuffer-project-name
          (let* ((proj-root (if (boundp 'elpy-project-root)
                                (elpy-project-root)
                              nil))
                 (buffer-path (or (buffer-file-name) default-directory ""))

                 (split-f-name (split-string (or proj-root buffer-path) ":"))
                 (f-path (car (last split-f-name)))
                 (conn (car split-f-name))
                 (proj-name
                  (cond
                   (proj-root
                    (let ((base-name (car (last (s-split "/" f-path) 2))))
                      (cond
                       ((s-equals? base-name "updates-server") (my-buffers-updates-subproj buffer-path))
                       (t base-name))))
                   ((string= f-path "") "unknown")
                   ((s-prefix? "/etc" f-path) "conf")
                   ((s-contains? "/Downloads" f-path) "downloads")
                   ((s-contains? "/tmp" f-path) "temp")
                   (t "unknown"))))
            (cond
             ((s-equals? conn "/sudo") (format "%s:ROOT" proj-name))
             ((s-equals? conn "/ssh")
              (if (s-prefix? "root@" (cadr split-f-name))
                  (format "%s:ROOT:remote" proj-name)
                (format "%s:remote" proj-name)))
             (t proj-name)))))
  my-ibuffer-project-name)

(define-ibuffer-column project (:name "Project" :inline t)
  "The project a buffer is a member of"
  (my-buffers-project-for-buffer))

(define-ibuffer-filter project
    "Toggle current view to buffers with project QUALIFIER."
  (:description "project"
   :reader (read-from-minibuffer "Filter by project (regexp): "))
  (string-match-p
   qualifier
   (or (buffer-local-value 'my-ibuffer-project-name buf)
       (with-current-buffer buf (my-buffers-project-for-buffer)))))

(define-ibuffer-sorter project
  "Sort the buffers by their project."
  (:description "project")
  (string-lessp
   (with-current-buffer (car a) (my-buffers-project-for-buffer))
   (with-current-buffer (car b) (my-buffers-project-for-buffer))
   ))

(define-ibuffer-sorter filename-or-dired
  "Sort the buffers by their pathname."
  (:description "filenames plus dired")
  (string-lessp
   (with-current-buffer (car a)
     (or buffer-file-name
         (if (eq major-mode 'dired-mode)
             (expand-file-name dired-directory))
         ;; so that all non pathnames are at the end
         "~"))
   (with-current-buffer (car b)
     (or buffer-file-name
         (if (eq major-mode 'dired-mode)
             (expand-file-name dired-directory))
         ;; so that all non pathnames are at the end
         "~"))))

(defun my-ibuffer-mode-hook ()
  (define-key ibuffer-mode-map (kbd "s P") 'ibuffer-do-sort-by-project)
  (define-key ibuffer-mode-map (kbd "s F") 'ibuffer-do-sort-by-filename-or-dired)
  (define-key ibuffer-mode-map (kbd "s V") 'ibuffer-do-sort-by-vc-status)

  (define-key ibuffer-mode-map (kbd "/ V") 'ibuffer-vc-set-filter-groups-by-vc-root)
  (define-key ibuffer-mode-map (kbd "/ P") 'ibuffer-filter-by-project)
  )
(add-hook 'ibuffer-mode-hook 'my-ibuffer-mode-hook)


;; The default C-x C-b is list-buffers, but ibuffer in my current window
;; makes me happier
(global-set-key (kbd "C-x C-b") 'ibuffer)

(global-set-key (kbd "M-S") 'search-all-buffers)
(global-set-key (kbd "C-c b") 'rename-buffer)

;; Revert the current buffer
(global-set-key [f9] 'revert-buffer)

;; Don't reuse frames when I ask to see a buffer
(if (or (and (<= emacs-major-version 24) (<= emacs-minor-version 1))
        (< emacs-major-version 24))
    (setq display-buffer-reuse-frames nil)
  (add-to-list 'display-buffer-alist
               `(,(rx bos "*grep*" eos)
                 (display-buffer-reuse-window
                  display-buffer-in-side-window)
                 (reusable-frames . visible)
                 (side            . bottom)
                 (window-height   . 0.4)))
  (add-to-list 'display-buffer-alist
             '("." nil (reusable-frames . t))))


(setq ido-default-buffer-method 'selected-window)
(setq ido-default-file-method 'selected-window)


(provide 'my-buffers)
