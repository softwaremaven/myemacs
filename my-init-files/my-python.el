;; Load paths
(require 'my-buffer-utilities)
(require 'my-code-utilities)

(setq elpy-rpc-pythonpath (concat dotfiles-dir "packages/elpy"))
(setq elpy-rpc-python-command "~/.virtualenvs/elpy/bin/python")
(add-to-list 'load-path elpy-rpc-pythonpath)
(setq elpy-rpc-backend "rope")

(require 'grep)
(require 'company)
(require 'find-file-in-project)
(require 'elpy)

(require 'pony-mode)
;;(require 'django-mode)
(require 'django-html-mode)
(require 'yasnippet)

(require 'info-look)

(require 'python)

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i")

(add-to-list 'grep-files-aliases '("py" . "*.py"))

;; Ignore the __pycache__ and pytest cache directories in find-file-in-project.
(add-to-list 'ffip-prune-patterns "*/__pycache__/*")
(add-to-list 'ffip-prune-patterns "*/.cache")
(add-to-list 'ffip-prune-patterns "*/.coverage*")

(add-to-list 'elpy-project-ignored-directories "__pycache__")
(add-to-list 'elpy-project-ignored-directories ".cache")
(add-to-list 'elpy-project-ignored-directories ".coverage*")
(add-to-list 'elpy-project-ignored-directories "*.pyc")
(add-to-list 'elpy-project-ignored-directories "*.egg-info")
(add-to-list 'elpy-project-ignored-directories "*.dist-info")

(info-lookup-add-help
 :mode 'python-mode
 :regexp "[[:alnum:]_]+"
 :doc-spec
 '(("(python)Index" nil "")))

(setq elpy-project-root-finder-functions
      '(elpy-project-find-projectile-root
        elpy-project-find-git-root
        elpy-project-find-hg-root
        elpy-project-find-python-root
        elpy-project-find-svn-root))

(elpy-enable)
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))

(setq python-check-command "flake8")

;; File bindings

(add-to-list 'interpreter-mode-alist '("python3*" . python-mode))
(add-to-list 'interpreter-mode-alist '("python2*" . python-mode))
(add-to-list 'interpreter-mode-alist '("kspython" . python-mode))
(add-to-list 'interpreter-mode-alist '("kspython-byname" . python-mode))

(setq auto-mode-alist (cons '("\\.djhtml$" . django-html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.html$" . django-html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.jinja$" . jinja2-mode) auto-mode-alist))

(autoload 'django-html-mode "django-html-mode" "Django template editing mode." t)

;; Macros
(fset 'tj-django-var     [?{ ?{ ?  ?  ?} ?} ?\C-b ?\C-b ?\C-b])
(fset 'tj-django-comment [?{ ?# ?  ?  ?# ?} ?\C-b ?\C-b ?\C-b])
(fset 'tj-django-tag     [?{ ?% ?  ?  ?% ?} ?\C-b ?\C-b ?\C-b])

(fset 'tj-traceback
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("!import traceback; traceback.print_exc()" 0 "%d")) arg)))
(fset 'tj-traceback2
  (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("import traceback; traceback.print_exc()" 0 "%d")) arg)))

; Highlight any calls to [i]pdb
; src http://pedrokroger.com/2010/07/configuring-emacs-as-a-python-ide-2/
(defun my-annotate-pdb ()
  (interactive)
  (highlight-lines-matching-regexp "import i?pdb")
  (highlight-lines-matching-regexp "i?pdb.set_trace()"))
(add-hook 'python-mode-hook 'my-annotate-pdb)

(defun my-pdb-cleanup ()
  (interactive)
  (save-excursion
    (replace-regexp ".*i?pdb.set_trace().*\n" "" nil (point-min) (point-max))
    ;; (save-buffer)
    ))

(defun turn-on-pony-mode-hook ()
  (if (pony-project-root)
      (pony-mode)))

;; Hooks for python and python-related modes
(when (require 'flycheck nil t)
 (add-hook 'elpy-mode-hook 'flycheck-mode))

; fix the keymap-clash between Pony and Elpy by making Pony's keymap use C-c M-p
(defun my-pony-minor-mode-hook ()
  (pony-key "\C-c\M-pb" 'pony-browser)
  (pony-key "\C-c\M-pd" 'pony-db-shell)
  (pony-key "\C-c\M-pf" 'pony-fabric)
  (pony-key "\C-c\M-pgt" 'pony-goto-template)
  (pony-key "\C-c\M-pgs" 'pony-goto-settings)
  (pony-key "\C-c\M-pr" 'pony-runserver)
  (pony-key "\C-c\M-pm" 'pony-manage)
  (pony-key "\C-c\M-ps" 'pony-shell)
  (pony-key "\C-c\M-p!" 'pony-shell)
  (pony-key "\C-c\M-pt" 'pony-test)
  (pony-key "\C-c\M-p\C-r" 'pony-reload-mode)
  )

;; Key Bindings
(defun my-python-mode-hook ()
  (electric-indent-mode -1)  ;; It is borked in python mode
  (setq comint-process-echoes t)
  (turn-on-pony-mode-hook)
  ;; And keybindings
  (define-key python-mode-map "\C-c\M-t" 'tj-traceback2)
)

(defun my-django-mode-hook ()
  (turn-on-pony-mode-hook)
  ;; Remove trailing spaces before saving
  (add-to-list 'write-file-functions 'delete-trailing-whitespace)
  (define-key django-html-mode-map "\C-cd" 'tj-django-tag)
  (define-key django-html-mode-map "\C-ct" 'django-html-insert-tag)
  (define-key django-html-mode-map "\C-cc" 'django-html-close-tag)
  (define-key django-html-mode-map "\C-c#" 'tj-django-comment)
  (define-key django-html-mode-map "\C-cv" 'tj-django-var)
  )

(add-hook 'elpy-mode-hook
          '(lambda ()
             (define-key elpy-mode-map (kbd "<S-return>") 'newline-below-point)
             (define-key elpy-mode-map (kbd "C-c C-f") 'projectile-find-file-dwim)))
(add-hook 'django-html-mode-hook 'my-django-mode-hook)
(add-hook 'python-mode-hook 'my-python-mode-hook)
(add-hook 'pony-minor-mode-hook 'my-pony-minor-mode-hook)
(add-hook 'shell-mode-hook
          '(lambda () (define-key shell-mode-map "\C-c\M-t" 'tj-traceback)))

(add-hook 'shell-mode-hook
          '(lambda () (define-key shell-mode-map "\C-cT" 'open-python-stack-entry)))
(add-hook 'term-mode-hook
          '(lambda () (define-key term-mode-map "\C-cT" 'open-python-stack-entry)))

(provide 'my-python)
