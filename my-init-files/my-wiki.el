
(add-hook 'mediawiki-mode-hook '(lambda() (visual-line-mode)))
(add-hook 'wiki-mode-hook '(lambda() (visual-line-mode)))

(provide 'my-wiki)
