;;(require 'rainbow-delimeters)
(require 'curry-compose)

(require 'my-elisp-utilities)

;; (add-hook 'clojure-mode-hook 'paredit-mode)
;; (add-hook 'lisp-mode-hook 'paredit-mode)
;; (add-hook 'racket-mode-hook 'paredit-mode)

;;(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
;;(add-hook 'lisp-mode-hook 'rainbow-delimiters-mode)
;;(add-hook 'racket-mode-hook 'rainbow-delimiters-mode)

(defun conditionally-enable-paredit-mode ()
  "Enable `paredit-mode' in the minibuffer, during `eval-expression'."
  (if (eq this-command 'eval-expression)
      (paredit-mode 1)))

;; (add-hook 'minibuffer-setup-hook 'conditionally-enable-paredit-mode)

(global-set-key (kbd "C-c e") 'eval-and-replace)
(global-set-key (kbd "M--") 'eval-expression)

(add-hook 'emacs-lisp-mode-hook 'remove-elc-on-save)
(defun my-lisp-mode-hook ()
  (define-key lisp-mode-shared-map (kbd "M-_") 'eval-region)
  (define-key lisp-mode-shared-map (kbd "C-M-z") 'indent-defun)
)

(add-hook 'emacs-lisp-mode-hook 'my-lisp-mode-hook)

(provide 'my-lisp)
