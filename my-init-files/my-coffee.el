
(require 'coffee-mode)

(defun coffee-custom ()
  "coffee-mode-hook"
  ; Set tab width as per coffee norms
  (set (make-local-variable 'tab-width) 2)

 ; Key bindings
 (define-key coffee-mode-map "\C-cc" 'coffee-compile-buffer)
 (define-key coffee-mode-map "\C-cz" 'coffee-repl)

 ;; Turn off *Messages* spam
 (setq coffee-debug-mode nil))

(add-hook 'coffee-mode-hook
  '(lambda() (coffee-custom)))

(provide 'my-coffee)