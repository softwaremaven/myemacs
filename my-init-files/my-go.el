(require 'go-mode-load)

(add-hook 'go-mode-hook '(setq indent-tabs-mode nil))
(add-hook 'go-mode-hook '(setq tab-width 4))
(provide 'my-go)
