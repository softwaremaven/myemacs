;; Load erlang stuff
(add-to-list 'load-path (concat dotfiles-dir "packages/distel/elisp"))
(add-to-list 'Info-directory-list  (concat dotfiles-dir "packages/distel/info"))


;; (cond
;;  ((string-match "linux" my-platform)
;;   (progn
;;     (setq erlang-root-dir "/usr")
;;     (setq exec-path (cons "/usr/bin" exec-path))))
;;  ((string-match "osx" my-platform)
;;   (progn
;;     (setq erlang-root-dir "/opt/local")
;;     (setq exec-path (cons "/usr/local/bin" exec-path)))))

(require 'erlang-start)
(require 'erlang-flymake)

(require 'distel)
(distel-setup)

;; Some Erlang customizations
(add-hook 'erlang-mode-hook
	  (lambda ()
	    ;; when starting an Erlang shell in Emacs, default in the node name
	    (setq inferior-erlang-machine-options '("-sname" "emacs"))
	    ;; add Erlang functions to an imenu menu
	    (imenu-add-to-menubar "imenu")))

;; A number of the erlang-extended-mode key bindings are useful in the shell too
(defconst distel-shell-keys
  '(("\C-\M-i"   erl-complete)
    ("\M-?"      erl-complete)	
    ("\M-."      erl-find-source-under-point)
    ("\M-,"      erl-find-source-unwind) 
    ("\M-*"      erl-find-source-unwind)
    )
  "Additional keys to bind when in Erlang shell.")
(add-def-keys-hook-for-mode erlang-shell-mode-hook erlang-shell-mode-map distel-shell-keys)

;; Use erlang's uncomment-region instead of the default
(defconst erlang-mode-keys
  '(("\C-c}" erlang-uncomment-region)
    )
  "Additional keys to bind when in Erlang mode."
  )
(add-def-keys-hook-for-mode erlang-mode-hook erlang-mode-map erlang-mode-keys)

(provide 'my-erlang)
