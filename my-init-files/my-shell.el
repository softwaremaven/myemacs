
(require 'my-shell-utilities)

(setq eshell-directory-name (concat dotfiles-dir "eshell"))

(setq term-buffer-maximum-size 65536)
(setq auto-mode-alist (cons '("^\\*shell\\*" . dirtrack-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("^\\*ansi-term\\*" . dirtrack-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("^\\*terminal\\*" . dirtrack-mode) auto-mode-alist))


;; Not super-keen on the way this is implemented. It would be better to
;; define a buffer-local variable on the shell command's buffer and test
;; for it. That could be done with something like:
;;
;;     ;; with-current-buffer may not be needed if the current buffer is set
;;     ;; by switch-to-buffer-other-window.
;;     (with-current-buffer output-buffer
;;       (setq-local my-erase-buffer-disabler t)
;;       (ad-activate-regexp "erase-buffer-noop")
;;       ad-do-it
;;       (ad-deactivate-regexp "erase-buffer-noop")
;;       (kill-local-variable my-erase-buffer-disabler))))))
;;
;; Then our erase-buffer advice body becomes:
;;
;; (unless (local-variable-p my-erase-buffer-disabler)
;;   ad-do-it
;; )


(global-set-key [M-f7] 'visit-term-buffer)
(global-set-key [f7] 'shell-dwim)

(setenv "PAGER" "cat")

(provide 'my-shell)
