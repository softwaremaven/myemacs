
;; Compilation
;;(setq compilation-scroll-output 1) ;; automatically scroll the compilation window
;;(setq compilation-window-height 10) ;; Set the compilation window height...
;; (setq compilation-finish-function  ;; Auto-dismiss compilation buffer...
;;       (lambda (buf str)
;;         (if (string-match "exited abnormally" str)
;;             (message "compilation errors, press F6 to visit")
;;           ; no errors, make the compilation window go away after 2.5 sec
;;           ; (run-at-time 2.5 nil 'delete-windows-on buf)
;;           (run-at-time 2.5 nil 'replace-buffer-in-windows buf)
;;           (message "No compilation errors!"))))


(defun my-prog-mode-compile-hook ()
  (require my-code-utilities)
  (define-key prog-mode-map (kbd "C-x m") 'my-compile-func)
  (define-key prog-mode-map (kbd "F5") 'my-compile-func)
  (define-key prog-mode-map (kbd "M-F5") 'my-compile-clean-func)
  (define-key prog-mode-map (kbd "C-F5") 'my-gdb))
(add-hook 'prog-mode-hook 'my-prog-mode-compile-hook)

(provide 'my-compile)
