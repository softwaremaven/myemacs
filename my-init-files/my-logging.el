;; Set up logging modes
(setq auto-mode-alist (cons '("\\.log$" . follow-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.log$" . auto-revert-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.out$" . follow-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.out$" . auto-revert-mode) auto-mode-alist))

(add-to-list 'auto-mode-alist '("\\.log\\'" . display-ansi-colors))

(provide 'my-logging)
