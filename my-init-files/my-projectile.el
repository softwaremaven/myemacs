
(require 'find-file-in-project)
(require 'projectile)
(require 'projectile-direnv)

(global-set-key (kbd "C-c C-f") 'projectile-find-file-dwim)
(global-set-key (kbd "C-c M-f") 'projectile-find-file-in-known-projects)

(setq my-projectile-ignored-project-prefixes '("/sudo:"))

(defun my-projectile-cache-project-advice (project files)
  "projectile-cache-project :before-while advice that provides the ability to ignore
certain projects (defined in the list `my-projectile-ignored-project-prefixes`)."
  (let ((no-ignored-prefixes? t))
    (dolist (prefix my-projectile-ignored-project-prefixes no-ignored-prefixes?)
      (setq no-ignored-prefixes? (and (not (s-starts-with? prefix project))
                                      no-ignored-prefixes?)))))
(advice-add 'projectile-cache-project :before-while 'my-projectile-cache-project-advice)
(advice-remove 'projectile-cache-project 'my-projectile-cache-project-advice )

(setq projectile-require-project-root t)
(setq projectile-enable-caching t)
(setq projectile-use-git-grep t)
;; To disable remote file exists cache (e.g. if the UI freezes
;; unacceptably), set this to nil.
(setq projectile-file-exists-remote-cache-expire (* 60 10))

(projectile-global-mode)

(provide 'my-projectile)
