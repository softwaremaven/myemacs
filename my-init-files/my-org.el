
(require 'ob-ipython)

(require 'my-note-utilities)

(global-set-key (quote [f8]) 'todays-notes)

(defun prep-for-present ()
  "Loads epresent"
  (interactive)
  (load-file (concat dotfiles-dir "packages/epresent/epresent.el"))
  )

(defun epresent-mode-prep ()
  (setq-local show-trailing-whitespace nil)
  )

(add-hook 'org-mode-hook 'turn-on-flyspell)
(add-hook 'epresent-mode-hook 'epresent-mode-prep)

(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "ditaa")))  ; don't ask for ditaa
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

(defun toggle-all-babel ()
  (interactive)
  (setq org-confirm-babel-evaluate
        (if (equal org-confirm-babel-evaluate nil)
            'my-org-confirm-babel-evaluate
          nil)))

(my-org-confirm-babel-evaluate "sh" "echo hi")

(provide 'my-org)
