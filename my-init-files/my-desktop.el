(require 'desktop)
(require 'workgroups2)
;;(require 'bookmark+)

(global-set-key (kbd "C-M-j") 'ace-jump-mode)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

(setq desktop-path (cons (concat dotfiles-dir "desktop") desktop-path))
(desktop-read)
(desktop-save-mode 1)

;; Enable C-c "left" and C-c "right" to cycle past window configurations
(when (fboundp 'winner-mode)
  (winner-mode 1))

;; Change prefix key (before activating WG)
;; (setq wg-prefix-key (kbd "C-c z"))

;; <prefix> <key>
;; <prefix> c    - create workgroup
;; <prefix> A    - rename workgroup
;; <prefix> k    - kill workgroup
;; <prefix> v    - switch to workgroup
;; <prefix> C-s  - save session
;; <prefix> C-f  - load session

;; Set your own keyboard shortcuts to reload/save/switch WG:
(global-set-key (kbd "M-S-<home>")  'wg-reload-session)
(global-set-key (kbd "M-S-<end>")   'wg-save-session)
(global-set-key (kbd "M-S-<prior>") 'wg-switch-to-workgroup)
(global-set-key (kbd "M-S-<next>")  'wg-switch-to-previous-workgroup)

(workgroups-mode 1)

(provide 'my-desktop)
