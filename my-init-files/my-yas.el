(require 'yasnippet)

(if (boundp 'yas-snippet-dirs)
    (let ((my-snippets (my-init-path "snippets")))
      (unless (member my-snippets yas-snippet-dirs)
        (add-to-list 'yas-snippet-dirs my-snippets)))
  (message "Warning: 'yas-snippet-dirs is not defined. It should be!"))

(defun dont-activate ()
  "Determines buffers yas shouldn't activate in"
  (or
   (s-prefix? "*" (buffer-name))  ; don't start for process buffers
  ))

(add-to-list 'yas-dont-activate-functions 'dont-activate)

(yas-global-mode 1)

(provide 'my-yas)
