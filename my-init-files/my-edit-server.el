
;(server-start)

(if (locate-library "edit-server")
    (progn
      (require 'edit-server)
      (setq edit-server-new-frame nil)
      (edit-server-start)
      (setq edit-server-url-major-mode-alist
            '(("github\\.com" . markdown-mode)))))

(provide 'my-edit-server)
