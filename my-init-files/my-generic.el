
;; Requires that don't need their own config files
(require 'centered-cursor-mode)
(require 'help-fns+)
(require 'browse-kill-ring+)

(transient-mark-mode 1)

;; Save our histories between sessions.
(savehist-mode 1)
(setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))
(setq savehist-file "~/.emacs.d/desktop/savehist")

;; Drop the bell. Hates it!
(setq ring-bell-function (lambda () nil))

;; Global keybindings
(define-key global-map "\M-`" 'other-frame) ;; Make like OS X

;; I've fat-fingered C-x C-c too many times.  Make C-x C-\ be the exit key
;;(global-unset-key "\C-x\C-c")
(global-set-key "\C-x\C-\\" 'kill-emacs)
(global-set-key "\C-x\C-c" 'clipboard-kill-ring-save)
(global-set-key "\C-x\C-v" 'clipboard-yank)

;; Make some aliases to M-x
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

;; Monkey with the UI
(setq-default column-number-mode t)
(setq-default line-number-mode t)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
;;(if (fboundp 'menu-bar-mode) (menu-bar-mode -1)) ;; This is too useful with JDE

;; Make searches case-insensitive
(setq case-fold-search t)

;; Set up browse-kill-ring+
(browse-kill-ring-default-keybindings)

;; return a backup file path of a give file path with full directory mirroring
;; from a root dir. non-existant dir will be created
;; (defun my-replace-in-string (string regexp newtext)
;;   (let ((skip (length newtext))
;;        (start 0))
;;     (while (string-match regexp string start)
;;       (setq string (replace-match newtext t t string)
;;            start (+ skip (match-beginning 0)))))
;;   string)

;; (defun my-backup-file-name (fpath)
;;  "Return a new file path of a given file path.
;; If the new path's directories does not exist, create them."
;;  (let (backup-root bpath)
;;    (setq backup-root "~/.emacs_backup/")
;;    (setq bpath (concat backup-root (my-replace-in-string fpath ":" "") "~"))
;;    (make-directory (file-name-directory bpath) bpath)
;;    bpath
;;  )
;; )
;; (setq make-backup-file-name-function 'my-backup-file-name)

(global-set-key (kbd "C-x O") 'previous-multiframe-window)

;; Press C-o during an isearch to hot-swap to occur
(define-key isearch-mode-map (kbd "C-o")
  (lambda () (interactive)
    (let ((case-fold-search isearch-case-fold-search))
      (occur (if isearch-regexp isearch-string (regexp-quote isearch-string))))))

(defun add-def-keys-hook-for-mode (mode-hook mode-map keys-spec)
  "Adds a hook to define a list of keys for a given mode hook. keys-spec is a list of tuples containing a key definition and a function to call"
  (add-hook 'mode-hook
            (lambda ()
              (dolist (spec keys-spec)
                (define-key mode-map (car spec) (cadr spec))))))

(provide 'my-generic)
