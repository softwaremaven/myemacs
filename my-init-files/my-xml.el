
;; XML files
(add-to-list 'auto-mode-alist '("\.xml$" . nxml-mode))

(provide 'my-xml)