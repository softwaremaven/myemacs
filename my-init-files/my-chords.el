(require 'key-chord)

(key-chord-define-global "BB" 'iswitchb)
(key-chord-define-global "FF" 'ido-find-file)
(key-chord-define-global "jk" 'beginning-of-buffer)
(key-chord-define-global "JJ" 'ido-switch-buffer)
(key-chord-define-global "jj" 'ace-window)
(key-chord-define-global "qw" 'other-window)
(key-chord-define-global "kk" 'just-one-space)
(key-chord-define-global "qq" 'previous-multiframe-window)

(key-chord-mode +1)

(provide 'my-chords)
