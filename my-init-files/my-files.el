(require 'recentf)

(require 'my-file-utilities)

;; Temporary File Handling
;; Make it so I don't have 'foo~' files all over the place.  Instead, put them
;; all into ~/.emacs_backup.
(defvar user-temporary-file-directory "~/.emacs_backup")
(defvar user-temporary-file-max-size (* 1024 1024 10))
(make-directory user-temporary-file-directory t)
(set-file-modes user-temporary-file-directory #o700)

(setq backup-by-copying t)
(setq backup-directory-alist
      `(("." . ,user-temporary-file-directory)
        (,tramp-file-name-regexp nil)))
(setq backup-enable-predicate
      '(lambda (name)
         (and (normal-backup-enable-predicate name)
              (not (or
                    (s-ends-with? ".log" name)
                    (s-ends-with? ".tar" name)
                    (s-ends-with? ".tar.gz" name)
                    (s-ends-with? ".tar.bz" name)))
              (let ((attrs (file-attributes name)))
                (and (eq (nth 2 attrs) (user-uid))
                     (< (nth 7 attrs) user-temporary-file-max-size))))))

(setq auto-save-list-file-prefix
      (concat user-temporary-file-directory "auto-saves-"))
(setq auto-save-file-name-transforms
      `((".*" ,user-temporary-file-directory t)))


(setq recentf-max-saved-items 200
      recentf-max-menu-items 15)
(recentf-mode +1)

(global-set-key (kbd "C-x C-r")  'recentf-ido-find-file)
(global-set-key (kbd "C-c M-d")  'delete-file-and-buffer)
(global-set-key (kbd "C-c r") 'rename-file-and-buffer)

(provide 'my-files)
