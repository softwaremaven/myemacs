
(global-set-key (kbd "M-?") 'complete-tag)

(defvar mytags-ctags "ctags" "Your Exuberant Ctags executable")
(defvar mytags-etags "etags" "Your etags executable")
(defvar mytags-file "TAGS" "Default name to use for tags files")
(defvar mytags-ctags-excludes "--exclude=3rdParty --exclude=dependencies --exclue=statusdev" "Patterns to exclude generating tags for.")
(defvar mytags-directories `(("ctag" (concat (getenv "HOME") "/src/updates-server"))
			     ; TODO: make this configurable outside of this .el file
			     ; ("ctag" (concat (getenv "HOME") "/ksplice-operations/python"))
			     ; ("ctag" (concat (getenv "HOME") "/ksplice-dev/python"))
			     ; ("ctag" (concat (getenv "HOME") "/ksplice-status"))
                             ("ctag" ,dotfiles-dir)
                             )
  "List of directories to generate tags for")
(defvar mytags-messages '("Patience is a virtue" "Not long now" "You need faster 'puter!" "Take a nap" "You looking at me?!" "What you talkin' 'bout, Willis?"))

(setq tags-table-list
      (list
       (concat (getenv "HOME") "/src/updates-server")))

(defun gen-tags-for-dir-pair (pair)
  "Helper for gen-tags-for-dir"
  (gen-tags-for-dir (car pair) (nth 1 pair)))

(defun gen-tags-for-dir (generator directory &optional prefix)
  "Generates tags for a set of programming languages for a DIRECTORY and all its subdirectories."
  (interactive "sTag generator (ctag, etag, etc): \nDTag directory: \nP")
  (message (format "Generating tags (%s) with '%s' for directory: %s (%s)"
                   mytags-file generator directory
                   (nth (random (length mytags-messages)) mytags-messages)))
  (let ((tag-file (format "%s/%s" directory mytags-file)))
    (cond ((string= generator "ctag")
           (shell-command
            (message
             (format
              "%s -f %s -e --recurse --extra=+q %s %s 2>/dev/null"
              mytags-ctags tag-file mytags-ctags-excludes directory))))
          ((string= generator "etag") (gen-tags-etag-dir tag-file directory))
          (t (message (format "Unknown tag generator: %s" generator))))))

(defun gen-tags-etag-dir (tag-file directory)
  "Generates (or regenerates) TAGFILE in a given DIRECTORY and its subdirectories using etags."
  (progn
    (condition-case nil
        (delete-file tag-file)
      (error nil))
    (mapc (lambda (x) (gen-tags-etag-an-entry tag-file directory x))
          (directory-files-and-attributes directory nil "^[^\.]") )
    )
)

(defun gen-tags-etag-an-entry (tag-file path entry)
  "Generates etags for an entry in the gen-tags-etag-dir subtree and puts them in TAG-FILE"
  (let ((entry-path (format "%s/%s" path (car entry)))
        (is-directory (nth 1 entry)))
    (if is-directory
        (mapc (lambda (x) (gen-tags-etag-an-entry tag-file entry-path x))
              (directory-files-and-attributes entry-path nil "^[^\.]") )
      (shell-command (format "%s -o %s -a %s 2>/dev/null"
                             mytags-etags tag-file entry-path))
      ))
)

(defun gen-tags ()
  "Generates tags for directories in mytags-directories"
  (interactive)
  (let ((tag-dirs mytags-directories))
    (mapc 'gen-tags-for-dir-pair tag-dirs)
    (message "Finished generating tags")))

(provide 'my-tags)
