;;
;; Functions and stuff that must be included before anything else is evaluated,
;; including platform files
;;

(defvar initial-frame-created-p (not (daemonp))
  "Has an initial frame been created yet?")
(defvar after-initial-frame-creation-hook nil
  "Set of things to do after we've actually created a frame")

(when initial-frame-created-p
  (message "initial frame already created; hooks will be run immediately"))

(defun add-frame-init-hook (hook &optional append)
  "Adds the given hook (function-ish object) to the initial frame hook list.
If a frame has already been created, the hook will be run immediately."
  (if initial-frame-created-p
      (progn
        (my-debug "running frame-init-hook function directly: %s" hook)
        (funcall hook))
    (my-debug "adding frame-init-hook function to hook list: %s\n%s"
              hook after-initial-frame-creation-hook)
    (add-to-list 'after-initial-frame-creation-hook
                 hook append)))

(defun run-initial-frame-hooks (&optional frame)
  (my-debug "running initial-frame-hooks")
  (unless initial-frame-created-p
    (my-debug "running initial frame hooks: %s" after-initial-frame-creation-hook)
    (mapcar (lambda (hook)
              (my-debug "Hook: %s" hook)
              (if (functionp hook) (funcall hook)
                (error "I don't know what this hook is: %s %s"
                       hook (type-of hook))))
            after-initial-frame-creation-hook)
    (setq initial-frame-created-p t)))

(add-hook 'after-make-frame-functions 'run-initial-frame-hooks)

(defun add-to-env-path (env path)
  (let ((curpath (getenv env)))
    (if (eq curpath nil)
        (setenv env path)
      (setenv env (concat curpath path-separator path)))))

(defun add-to-exec-path (path)
  (setq exec-path (append (list path) exec-path)))

(menu-bar-mode -1)
(setq set-mark-command-repeat-pop t)

;; (electric-pair-mode +1)
(electric-indent-mode +1)

(add-to-list 'auto-mode-alist '("\\.pp$" . puppet-mode))

(provide 'my-init-0)
