(require 'my-internet-utilities)

(global-set-key (kbd "C-c C-w") 'browse-url-at-point)
(global-set-key (kbd "C-c C-s") 'my-search-internet)
(global-set-key (kbd "C-c w g") 'my-search-google)
(global-set-key (kbd "C-c w a") 'my-search-apple)
(global-set-key (kbd "C-c w s") 'my-search-so)
(global-set-key (kbd "C-c w e") 'my-search-emacs)
(global-set-key (kbd "C-c w w") 'my-search-wikipedia)
(global-set-key (kbd "C-c w 3") 'my-search-w3schools)
(global-set-key (kbd "C-c w h") 'my-search-hn)

(provide 'my-web)
