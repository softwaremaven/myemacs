(require 'my-file-utilities)

;; Look for passwords and don't show them when typing
;;(add-hook 'comint-output-filter-functions
;;          'comint-watch-for-password-prompt)

;; Make shells more useful
;; M-x local-set-key RET C-z self-insert-command

(ansi-color-for-comint-mode-on)

(global-set-key (kbd "C-x M-r")  'sudo-edit)
(global-set-key (kbd "C-x M-!")  'sudo-shell-command)

(provide 'my-comint)
