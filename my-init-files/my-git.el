(require 'my-vcs-utilities)

(add-to-list 'load-path (concat dotfiles-dir "packages/egg"))

(require 'egg)
(require 'magit)

(global-set-key (kbd "M-s") 'my-maybe-git-rgrep)
(global-set-key (kbd "C-x g") 'magit-status)

(provide 'my-git)
