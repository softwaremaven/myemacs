
(require 'my-buffer-utilities)
(require 'my-code-utilities)
(require 'mediawiki-mode)
(require 'wiki-mode)

(defun my-add-fci-prog-hook ()
  (unless (fboundp 'fci-mode)
    (require 'fill-column-indicator))
  (setq-local fci-rule-color "yellow")
  ;; enable the right-margin indicator
  (fci-mode 1))

;; We can't enable fci-mode until after we have a frame.
(add-frame-init-hook
 (lambda ()
   ;; Make sure any existing buffers have the right values.
   (do-for-all-buffers 'my-add-fci-prog-hook
                       (apply-partially 'derived-mode-p 'prog-mode))
   (add-hook 'prog-mode-hook 'my-add-fci-prog-hook)))

;; Auto-indent for programming modes that aren't smart already
(setq my-prog-like-modes-list
      (list 'prog-mode 'sgml-mode 'yaml-mode 'mediawiki-mode 'wiki-mode))
(mapc
 (lambda (the-mode)
   (require the-mode)
   (let ((mode-hook (intern (concat (symbol-name the-mode) "-hook"))))
     (if (boundp mode-hook)
         (add-hook (symbol-value 'mode-hook)
                   (lambda ()
                     (setq-local fill-column 85)
                     ;; Remove tabs from file when saving.
                     ;; (make-local-variable 'write-contents-hooks)
                     (add-hook 'write-contents-functions 'untabify-buffer)
                     ;; Remove trailing spaces before saving
                     (add-hook 'write-contents-functions 'delete-trailing-whitespace)
                     (define-key (current-local-map) (kbd "C-c }")  'comment-region)
                     (define-key (current-local-map) (kbd "C-c {")  'uncomment-region)
                     (define-key (current-local-map) (kbd "C-c #") 'comment-region)
                     (define-key (current-local-map) (kbd "<S-return>") 'smart-open-line)
                     (define-key (current-local-map) (kbd "RET") 'newline-and-indent)))
       (message "mode-hook %s not bound" mode-hook))))
 my-prog-like-modes-list)

(defun my-prog-mode-hook ()
  ;; Technically, this is for more than prog-mode's, but I want it this way
  ;; for most cases, and those that I don't can get set independently.
  ;; Set up tab spaces
  (setq-local c-default-style "bsd")
  (setq-local c-basic-offset 4)
  ;; enable flyspell in code comments
  (flyspell-prog-mode)
  )
(add-hook 'prog-mode-hook 'my-prog-mode-hook)

(provide 'my-prog-mode)
