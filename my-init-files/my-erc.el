
(load "~/.ercpass" t)
(require 'erc-services)
(require 'my-growl-utilities)

(erc-services-mode 1)

;; (mapcar (lambda (server-ident)
;;           (let* ((server   (make-symbol (car server-ident)))
;;                  (user     (cdr server-ident))
;;                  (pass-sym (make-symbol (format "%s-%s-pass" server user)))
;;                  (passwd   (when (boundp pass-sym)) (symbol-value pass-sym)))
;;             (my-debug "ERC mode setup for %s: user is %s; password is %i characters"
;;                       server user (if passwd (length passwd) -1))
;;             ;; Can't just "push" here. We need to see if `server` already exists in
;;             ;; `erc-nickserv-passwords`. If it does, we need to "push" onto its list;
;;             ;; otherwise, we need to create a new list for it.
;;             (push ` 'erc-nickserv-passwords)))
;;         '(("freenode" . "NitFlickwick")))

(if (boundp 'freenode-nitflickwick-pass)
    (progn
      (setq erc-prompt-for-nickserv-password nil)
      (setq erc-nickserv-passwords
            `((freenode     (("NitFlickwick" . ,freenode-nitflickwick-pass))))
            )))

;; Set up growlnotify
(defun my-erc-hook (match-type nick message)
  "Shows a growl notification, when user's nick was mentioned. If the buffer is currently not visible, makes it sticky."
  (unless (posix-string-match "^\\** *Users on #" message)
    (growl
     (concat "ERC: name mentioned on: " (buffer-name (current-buffer)))
     message
     )))

;; Use w3m for ERC unless told otherwise
;; (require 'w3m-load)
;; (require 'w3m)

;; (defun browse-url-default-macosx-browser (url &optional in-safari)
;;   (interactive (browse-url-interactive-arg "URL: "))
;;   (if (and in-safari (>= emacs-major-version 23))
;;       (ns-do-applescript
;;        (format (concat "tell application \"Safari\" to make document with properties {URL:\"%s\"}\n"
;;                     "tell application \"Safari\" to activate") url))
;;     (start-process (concat "open " url) nil "open" url)))

;; (defun choose-browser (url &rest args)
;;   (interactive "sURL: ")
;;   (if (y-or-n-p "Use external browser? ")
;;       (browse-url-generic url)
;;     (w3m-browse-url url)))

;; (setq browse-url-browser-function 'browse-url-default-macosx-browser)
;; (setq browse-url-generic-program "/Applications/Google Chrome.app")
;; (setq browse-url-browser-function 'choose-browser)

(add-hook 'erc-text-matched-hook 'my-erc-hook)

;; to modify it to suit your needs.
(defun my-irc ()
  "Start to waste time on IRC with ERC."
  (interactive)
  (select-frame (make-frame '((name . "Emacs IRC")
                              (minibuffer . t))))
  (call-interactively 'erc-ircnet)
  (sit-for 1)
  (call-interactively 'erc-opn)
  (sit-for 1)
  (call-interactively 'erc-ifs))

;; Don't show join/part/quit messages.  Too much noise
(setq erc-hide-list '("JOIN" "PART" "QUIT"))

;; Set my auto-start channels. For multiple servers, add another list of
;; the form ("theserver.net" "#channel1" "#channel2" ...)
(setq erc-autojoin-channels-alist
      '(
        ("freenode.net" "#emacs" "#python" "#utah" "#erlang" "#couchdb")
        ))

(provide 'my-erc)
