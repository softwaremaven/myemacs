(require 'my-selection-utilities)
(require 'my-text-utilities)

;;(add-hook 'text-mode-hook '(lambda() (visual-line-mode 1)))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(global-hl-line-mode +1)
(delete-selection-mode +1)

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

;; Tabs Stuff
(setq tab-width 4)
(setq fill-column 76)
(setq-default indent-tabs-mode nil)

(global-set-key (kbd "C-c j") 'just-one-space)

(global-set-key [(meta shift up)]  'move-line-up)
(global-set-key [(meta shift down)]  'move-line-down)

;; Make editing faster
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "C-x C-k") 'kill-region)
(global-set-key (kbd "C-c C-k") 'kill-region)

;; Make some things faster and easier
(global-set-key (kbd "M-g") 'goto-line)
(global-set-key (kbd "C-x j") 'join-line)

(global-set-key (kbd "C-M-y") 'my-yank-primary)

;; easy spell check
(global-set-key (kbd "C-c C-y s") 'ispell-word)
(global-set-key (kbd "C-c C-y c") 'flyspell-prog-mode)
(global-set-key (kbd "C-c C-y f") 'flyspell-mode)
(global-set-key (kbd "C-c C-y b") 'flyspell-buffer)
(global-set-key (kbd "C-c C-y p") 'flyspell-check-previous-highlighted-word)
(defun flyspell-check-next-highlighted-word ()
    "Custom function to spell check next highlighted word"
      (interactive)
        (flyspell-goto-next-error)
          (ispell-word)
            )
(global-set-key (kbd "C-c C-y n") 'flyspell-check-next-highlighted-word)

(add-hook 'text-mode-hook
          (lambda ()
            (turn-on-visual-line-mode)
            ;; Remove trailing spaces before saving
            (add-hook 'write-contents-functions 'delete-trailing-whitespace)
            (setq-local truncate-lines 100)))

;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)
(global-set-key (kbd "C-M-\\") 'indent-region-or-buffer)

;; The original value is "\f\\|[      ]*$", so we add the bullets (-), (+), and (*).
;; There is no need for "^" as the regexp is matched at the beginning of line.
(setq paragraph-start
      (rx-to-string
       '(or "\f"
            (and (0+ blank)
                 (or line-end
                     (and (any "-+*") " ")
                     (and "|" (zero-or-one "-") (0+ blank))
                     (and (one-or-more digit) "." blank)
                     (and (repeat 1 2 hex-digit)
                          (repeat 0 3
                                  (and (any ".:") (repeat 0 2 hex-digit)))
                          (or (any ".:")
                              (and (zero-or-more blank) "-"))))))
       t))


(provide 'my-text)
