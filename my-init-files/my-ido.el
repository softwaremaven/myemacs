(setq ido-default-buffer-method "selected-window")

(defun my-ido-keys ()
  (define-key ido-completion-map (kbd "C-.") 'ido-next-match)
  (define-key ido-completion-map (kbd "C-,") 'ido-prev-match)
)
(add-hook 'ido-setup-hook 'my-ido-keys)

(ido-mode t)

(provide 'my-ido)
