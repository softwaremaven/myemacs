;; 
;; Superb way to move between Emacs windows
;; Shift+arrow keys
(windmove-default-keybindings)
(provide 'my-windmove)